/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * opl4.c
 */

#include "opl4.h"

#define OPL4_WAVE_CHANNELS 24
#define OPL4_MAX_RAM_BANK  64
#define OPL4_MAGIC_NUMBER  123
#define OPL4_MAGIC_NUMBER_INVERT 321

#define OPL4_WAIT    while(opl4_fm_base & 0x01);
#define OPL4_WAIT_LD while(opl4_fm_base & 0x02);

#define OPL4_TIMER1_COUNT  0x02
#define OPL4_TIMER2_COUNT  0x03
#define OPL4_TIMER_CONTROL 0x04

/*
 * OUT ports used for OPL4
*/
__sfr __at 0xC4 opl4_fm_base;
__sfr __at 0x7E opl4_wave_base;
__sfr __at 0xC4 opl4_status;

__sfr __at 0xC4 opl4_fm_reg1;
__sfr __at 0xC5 opl4_fm_data1;
__sfr __at 0xC6 opl4_fm_reg2;
__sfr __at 0xC7 opl4_fm_data2;

__sfr __at 0x7E opl4_wave_reg;
__sfr __at 0x7F opl4_wave_data;


bool opl4_detect()
{
  return (opl4_fm_base == 0xFF) ? false : true;
}

void opl4_reset()
{
  if(opl4_detect())
  {
    /* Set to OPL4 mode */
    opl4_write_fm_register_array_2(0x05, 0x03);

    /* Reset FM registers */
    opl4_write_fm_register_array_1(0x01, 0x00);
    opl4_write_fm_register_array_1(0x02, 0x00);
    opl4_write_fm_register_array_1(0x03, 0x00);
    opl4_write_fm_register_array_1(0x04, 0x00);
    opl4_write_fm_register_array_1(0x08, 0x00);

    opl4_write_fm_register_array_2(0x01, 0x00);
    opl4_write_fm_register_array_2(0x02, 0x00);
    opl4_write_fm_register_array_2(0x03, 0x00);
    opl4_write_fm_register_array_2(0x04, 0x00);
    opl4_write_fm_register_array_2(0x08, 0x00);

    for (uint8_t i = 0x14; i < 0xF6; i++)
    {
      uint8_t value = (i >= 0x60 && i < 0xA0) ? 0xFF : 0;
      opl4_write_fm_register_array_1(i, value);
      opl4_write_fm_register_array_2(i, value);
    }

    /* Set mix control */
    opl4_write_wave_register(0xF8, 0x1B);
    opl4_write_wave_register(0xF9, 0x00);

    /* Reset WAVE registers */
    for (uint8_t i = 0; i < OPL4_WAVE_CHANNELS; i++)
    {
      opl4_write_wave_register(0x68 + i, 0x40);
    }

    /* Reset timer flags */
    opl4_write_fm_register_array_1(0x04, 0x80);
  }
}

uint8_t opl4_sample_ram_banks()
{
  uint8_t result = 0;

  /* Set custom sample headers to 16Mb area and READ/WRITE mode */
  opl4_write_wave_register(0x02, 0x11);

  /* Detect the number of 64K banks of RAM available */
  for (uint8_t i = 0; !result && i <= OPL4_MAX_RAM_BANK; i++)
  {
    /* Write block number to corresponding OPL4 RAM address */
    opl4_write_wave_register(0x03, i + 0x20);               /* Register 3: memory addres bits 16-21 */
    opl4_write_wave_register(0x04, 0x00);                   /* Register 4: memory addres bits 08-15 */
    opl4_write_wave_register(0x05, 0x00);                   /* Register 5: memory addres bits 00-07 */
    opl4_write_wave_register(0x06, i + OPL4_MAGIC_NUMBER);

    opl4_write_wave_register(0x03, i + 0x20);               /* Register 3: memory addres bits 16-21 */
    opl4_write_wave_register(0x04, 0x00);                   /* Register 4: memory addres bits 08-15 */
    opl4_write_wave_register(0x05, 0x01);                   /* Register 5: memory addres bits 00-07 */
    opl4_write_wave_register(0x06, i + OPL4_MAGIC_NUMBER_INVERT);

    /* Read back the number from the same location and check */
    opl4_write_wave_register(0x03, i + 0x20);               /* Register 3: memory addres bits 16-21 */
    opl4_write_wave_register(0x04, 0x00);                   /* Register 4: memory addres bits 08-15 */
    opl4_write_wave_register(0x05, 0x00);                   /* Register 5: memory addres bits 00-07 */

    if (opl4_read_wave_register(0x06) != (i + OPL4_MAGIC_NUMBER))
    {
      result = i;
      break;
    }


    /* Check if one of the earlier found banks has been overwritten */
    for (int8_t j = i; j >= 0; j--)
    {
      /* Read back the number from the same location and check */
      opl4_write_wave_register(0x03, j + 0x20);           /* Register 3: memory addres bits 16-21 */
      opl4_write_wave_register(0x04, 0x00);               /* Register 4: memory addres bits 08-15 */
      opl4_write_wave_register(0x05, 0x00);               /* Register 5: memory addres bits 00-07 */

      if (opl4_read_wave_register(0x06) != (j + OPL4_MAGIC_NUMBER))
      {
        result = i;
        break;
      }
    }
  }

  /* Set custom sample headers to 16Mb area and PLAY mode*/
  opl4_write_wave_register(0x02, 0x10);

  return result;
}

void opl4_set_refresh(float refresh)
{ 
if (refresh > 50.0)
{
  opl4_write_fm_register_array_1(OPL4_TIMER1_COUNT, 255 - (uint8_t)((1000.0 / refresh) / 0.08));
  opl4_write_fm_register_array_1(OPL4_TIMER_CONTROL, 0x21);
  opl4_write_fm_register_array_1(OPL4_TIMER_CONTROL, 0x80);
}
else
{
  opl4_write_fm_register_array_1(OPL4_TIMER2_COUNT, 256 - (uint8_t)((1000.0 / refresh) / 0.320));
  opl4_write_fm_register_array_1(OPL4_TIMER_CONTROL, 0x42);
  opl4_write_fm_register_array_1(OPL4_TIMER_CONTROL, 0x80);
}
}

void opl4_write_fm_register_array_1(const uint8_t reg, const uint8_t value)
{
  opl4_fm_reg1 = reg;

  OPL4_WAIT;
  opl4_fm_data1 = value;
}

void opl4_write_fm_register_array_2(const uint8_t reg, const uint8_t value)
{
  opl4_fm_reg2 = reg;

  OPL4_WAIT;
  opl4_fm_data2 = value;
}

void opl4_write_wave_register(const uint8_t reg, const uint8_t value)
{
  opl4_wave_reg = reg;

  OPL4_WAIT;
  opl4_wave_data = value;
}

void opl4_write_wave_data(uint8_t *const data, const uint16_t size)
{
  opl4_wave_reg = 6;

  OPL4_WAIT;
  for (uint16_t i = 0; i < size; i++)
  {
    opl4_wave_data = data[i];
  }
}

uint8_t opl4_read_status_register()
{
  return (uint8_t)opl4_fm_base;
}

uint8_t opl4_read_fm_register_array_1(const uint8_t reg)
{
  opl4_fm_reg1 = reg;

  OPL4_WAIT
  return opl4_fm_data1;
}

uint8_t opl4_read_fm_register_array_2(const uint8_t reg)
{
  opl4_fm_reg2 = reg;

  OPL4_WAIT
  return opl4_fm_data2;
}

uint8_t opl4_read_wave_register(const uint8_t reg)
{
  opl4_wave_reg = reg;
  OPL4_WAIT;
  return (uint8_t)opl4_wave_data;
}

void opl4_wait_for_ld()
{
  OPL4_WAIT_LD;
}
