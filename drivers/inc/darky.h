/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * darky.h
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>

bool darky_detect();
uint8_t darky_get_version();

void darky_reset();
void darky_restore();

void epsg1_write(const uint8_t reg, const uint8_t value);
uint8_t epsg1_read(const uint8_t reg);

void epsg2_write(const uint8_t reg, const uint8_t value);
uint8_t epsg2_read(const uint8_t reg);

