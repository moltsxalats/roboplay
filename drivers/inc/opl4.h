/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * opl4.h
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

bool opl4_detect();
void opl4_reset();

uint8_t opl4_sample_ram_banks();

void opl4_set_refresh(float refresh);

void opl4_write_fm_register_array_1(const uint8_t reg, const uint8_t value);
void opl4_write_fm_register_array_2(const uint8_t reg, const uint8_t value);

void opl4_write_wave_register(const uint8_t reg, const uint8_t value);
void opl4_write_wave_data(uint8_t *const data, const uint16_t size);

uint8_t opl4_read_status_register();
uint8_t opl4_read_fm_register_array_1(const uint8_t reg);
uint8_t opl4_read_fm_register_array_2(const uint8_t reg);
uint8_t opl4_read_wave_register(const uint8_t reg);

void opl4_wait_for_ld();
