/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * vdp.h
 */

#pragma once

#include <stdint.h>

#define VDP_REFRESH_60_HZ 0
#define VDP_REFRESH_50_HZ 1

void init_vdp_device();

uint8_t get_vdp_refresh();
uint8_t vdp_read_status_register();