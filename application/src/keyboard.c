/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 * 
 * keyboard.c
 */

#include "keyboard.h"

__sfr __at 0xA8 ppi_reg_a;
__sfr __at 0xA9 ppi_reg_b;
__sfr __at 0xAA ppi_reg_c;

bool is_key_pressed_ESC()
{
    ppi_reg_c = (ppi_reg_c & 0xF0) | 0x07;
    return (ppi_reg_b & 0x04) ? false : true;
}

bool is_key_pressed_LEFT()
{
    bool result = false;

    ppi_reg_c = (ppi_reg_c & 0xF0) | 0x08;
    if(!(ppi_reg_b & 0x10))
    {
        result = true;
        while(!(ppi_reg_b & 0x10));
    }

    return result;
}

bool is_key_pressed_RIGHT()
{
    bool result = false;

    ppi_reg_c = (ppi_reg_c & 0xF0) | 0x08;
    if(!(ppi_reg_b & 0x80))
    {
        result = true;
        while(!(ppi_reg_b & 0x80));
    }

    return result;
}

bool is_key_pressed_SPACE()
{
    bool result = false;

    ppi_reg_c = (ppi_reg_c & 0xF0) | 0x08;
    if(!(ppi_reg_b & 0x01))
    {
        result = true;
        while(!(ppi_reg_b & 0x01));
    }

    return result;
}

bool is_number_key_pressed(const uint8_t number)
{
    bool result = false;

    ppi_reg_c = (ppi_reg_c & 0xF0) | 0x00;
    if(!(ppi_reg_b & (1 << number)))
    {
        result = true;
//        while(!(ppi_reg_b & (1 << number)));
    }

    return result;
}
