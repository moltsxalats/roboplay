/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * player.h
 */

#pragma once

void find_player_name();
void init_player();
