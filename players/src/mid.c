/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * mid.c
 *
 * MID: Standard MIDI file player on OPL4
 */

#include <string.h>

#include "data/inc/yrw801.h"
#include "drivers/inc/opl4_defines.h"

#include "player_interface.h"
#include "mid.h"

bool load(const char* file_name)
{
    g_roboplay_interface->open(file_name, false);

    /* Load the MIDI file header */
    uint8_t *file_data = (uint8_t *)READ_BUFFER;
    g_roboplay_interface->read(file_data, MIDI_HEADER_SIZE);
    if(file_data[0] != 'M' || file_data[1] != 'T' && file_data[2] != 'h' && file_data[3] != 'd' ||
       file_data[4] != 0 || file_data[5] != 0 || file_data[6] != 0 || file_data[7] != 6 )
    {
        return false;
    }
    if(file_data[8] != 0) return false;
    g_header.file_format = file_data[9];
    if(g_header.file_format > 2) return false;

    g_header.number_of_tracks = file_data[10];
    g_header.number_of_tracks <<= 8;
    g_header.number_of_tracks |= file_data[11];

    g_header.ticks_per_qnote = file_data[12];
    g_header.ticks_per_qnote <<= 8;
    g_header.ticks_per_qnote |= file_data[13];

    if(g_header.ticks_per_qnote < 1) return false;

    /* Load MIDI track data */
    load_track_data();

    g_roboplay_interface->close();

    /* Load YRW801 wave table */
    load_wave_data();

    return true;
}

bool update()
{
    bool song_finished = true;

    g_MIDI_counter += g_ticks_per_update;
    for(g_track = 0; g_track < g_header.number_of_tracks; g_track++)
    {
        if(!g_track_data[g_track].track_finished)
        {
            g_roboplay_interface->set_segment(g_track_data[g_track].segment);
            while(g_MIDI_counter >= g_track_data[g_track].waiting_for)
            {
                handle_track_event();
                get_delta_time();
            }
        }
        if(!g_track_data[g_track].track_finished) song_finished = false;
    }

    return !song_finished;
}

void rewind(int8_t subsong)
{
    /* No subsongs in this format */
    subsong;

    /* Reset tempo to 120BPM */
    g_qnote_duration = 500000;

    g_MIDI_counter = 0;
    g_volume_boost = 16;

    for(g_track = 0; g_track < g_header.number_of_tracks; g_track++)
    {
        g_track_data[g_track].segment = g_track_data[g_track].start_segment;
        g_track_data[g_track].track_data = g_track_data[g_track].start_track_data;

        g_track_data[g_track].track_finished = false;
        g_track_data[g_track].waiting_for = 0;
        g_track_data[g_track].last_command = 0xFF;

        g_roboplay_interface->set_segment(g_track_data[g_track].segment);
        get_delta_time();
    }

    for(uint8_t channel = 0; channel < NR_OF_MIDI_CHANNELS; channel++)
    {
        g_midi_channel_data[channel].instrument = 0;
        g_midi_channel_data[channel].panpot     = 0;
        g_midi_channel_data[channel].vibrato    = 0;
        g_midi_channel_data[channel].drum_channel = (channel == DRUM_CHANNEL);
    }

    for(uint8_t voice = 0; voice < NR_OF_WAVE_CHANNELS; voice++)
    {
        g_voice_data[voice].number = voice;
        g_voice_data[voice].is_active = false;
        g_voice_data[voice].activated = 0;
        g_voice_data[voice].midi_channel = NULL;
        g_voice_data[voice].note = 0;
        g_voice_data[voice].velocity = 0;
        g_voice_data[voice].wave_data = NULL;
        g_voice_data[voice].level_direct = 0;
        g_voice_data[voice].reg_f_number = 0;
        g_voice_data[voice].reg_misc = 0;
        g_voice_data[voice].reg_lfo_vibrato = 0;
    }
}

void command(const uint8_t id)
{
    /* No additional commmands supported */
    id;
}

float get_refresh()
{
    /* Use a fixed replay rate of 80 Hz */
    static const float rate = 100.0;

    float tick_duration = g_qnote_duration / g_header.ticks_per_qnote;
    float update_duration  = 1000000.0 / rate;
    float ticks_per_update = 0.5 + (update_duration / tick_duration);

    g_ticks_per_update = ticks_per_update;

    return rate;
}

uint8_t get_subsongs()
{
    return 0;
}

char* get_player_info()
{
    return "Standard MIDI file player (Wave) V1.1 by RoboSoft Inc.";
}

char* get_title()
{
    return "-";
}

char* get_author()
{
    return "-";
}

char* get_description()
{
    switch(g_header.file_format)
    {
        case 0x00:
            return "SMF format 0";
            break;

        case 0x01:
            return "SMF format 1";
            break;

        case 0x02:
            return "SMF format 2";
            break;

        default:
            return "-";            
    }

}

void load_track_data()
{
    uint8_t  *destination     = (uint8_t *)DATA_SEGMENT_BASE;
    uint16_t  page_left       = DATA_SEGMENT_SIZE;
    uint8_t   current_segment = START_SEGMENT_INDEX;

    for(uint8_t i = 0; i < g_header.number_of_tracks; i++)
    {
        g_track_data[i].start_segment = current_segment;
        g_track_data[i].start_track_data = destination;

        uint8_t track_buffer[4];
        g_roboplay_interface->read(track_buffer, 4);  /* MTrk */
        g_roboplay_interface->read(track_buffer, 4);  /* Header Length */

        g_track_data[i].length = track_buffer[0];
        g_track_data[i].length <<= 8;
        g_track_data[i].length |= track_buffer[1];
        g_track_data[i].length <<= 8;
        g_track_data[i].length |= track_buffer[2];
        g_track_data[i].length <<= 8;
        g_track_data[i].length |= track_buffer[3];

        uint32_t data_size = g_track_data[i].length;
        while(data_size > 0)
        {
            uint16_t read_size = (data_size < READ_BUFFER_SIZE) ? data_size : READ_BUFFER_SIZE;
            read_size = (read_size < page_left) ? read_size : page_left;

            /* It's not possible to read directly to non-primary mapper memory segments,
               so use a buffer inbetween. */
            uint16_t bytes_read = g_roboplay_interface->read((void*)READ_BUFFER, read_size);
            memcpy(destination, (void*)READ_BUFFER, bytes_read);

            data_size -= bytes_read;
            destination += bytes_read;
            page_left -= bytes_read;
            if(page_left == 0)
            {
                current_segment = g_roboplay_interface->get_new_segment();
                g_roboplay_interface->set_segment(current_segment);

                page_left = DATA_SEGMENT_SIZE;
                destination = (uint8_t *)DATA_SEGMENT_BASE;
            }
        }
    }
}

void load_wave_data()
{
    g_wave_segment = g_roboplay_interface->get_new_segment();
    g_roboplay_interface->set_segment(g_wave_segment);

    g_roboplay_interface->open("YRW801.DAT", true);

    uint8_t *destination = (uint8_t *)DATA_SEGMENT_BASE;
    uint16_t bytes_read = 0;
    do
    {
        /* It's not possible to read directly to non-primary mapper memory segments,
           so use a buffer inbetween. */
        bytes_read = g_roboplay_interface->read((void*)READ_BUFFER, READ_BUFFER_SIZE);
        if(bytes_read)
        {
            memcpy(destination, (void*)READ_BUFFER, bytes_read);
            destination += bytes_read;
        }
    } while(bytes_read);

    g_roboplay_interface->close();
}

uint8_t read_byte()
{
    uint8_t data = *(g_track_data[g_track].track_data++);

    if(g_track_data[g_track].track_data == (uint8_t *)(DATA_SEGMENT_BASE + DATA_SEGMENT_SIZE))
    {
        g_track_data[g_track].segment++;
        g_roboplay_interface->set_segment(g_track_data[g_track].segment);
        g_track_data[g_track].track_data = (uint8_t*)DATA_SEGMENT_BASE;
    }

    return data;
}

uint32_t get_variable_len()
{
    uint8_t data;
    uint32_t value = 0;

    do
    {
        data = read_byte();
        value = (value << 7) + (data & 0x7F);
    } while (data & 0x80);

    return value;
}

void get_delta_time()
{
    g_track_data[g_track].waiting_for += get_variable_len();
}

void change_speed(uint32_t speed)
{
    g_qnote_duration = speed;
    g_roboplay_interface->update_refresh();
}

void handle_track_event()
{
    uint8_t data_byte_1, data_byte_2, data_byte_3;

    data_byte_1 =  read_byte();

    /* If MSB is not set, this is a running status */
    if((data_byte_1 & 128) != 0)
    {
        g_track_data[g_track].last_command = data_byte_1;
        data_byte_2 = read_byte();
    }
    else
    {
        data_byte_2 = data_byte_1;
        data_byte_1 = g_track_data[g_track].last_command;
    }

    if(data_byte_1 == 0xFF)
        handle_meta_event(data_byte_2);
    else if(data_byte_1 == 0xF0)
        handle_sys_ex_event();
    else
    {
        uint8_t midi_channel = data_byte_1 & 0x0F;
        uint8_t midi_command = data_byte_1 >> 4;

        switch(midi_command)
        {
            case 0x8:  /* Note OFF */
                data_byte_3 = read_byte();
                note_off(data_byte_2 & 0x7F, data_byte_3, &g_midi_channel_data[midi_channel]);
                break;
            case 0x9:  /* Note ON */
                data_byte_3 = read_byte();
                note_on(data_byte_2 & 0x7F, data_byte_3, &g_midi_channel_data[midi_channel]);
                break;
            case 0xA:  /* Key after-touch */
                data_byte_3 = read_byte();
                break;
            case 0xB:  /* Control change */
                data_byte_3 = read_byte();
                control_change(midi_channel, data_byte_2, data_byte_3);
                break;
            case 0xC:  /* Program change */
                program_change(midi_channel, data_byte_2 & 0x7F);
                break;
            case 0xD:  /* Channel after-touch */
                data_byte_2 = read_byte();               
                break;
            case 0xE:  /* Pitch wheel */
                data_byte_3 = read_byte();
                pitch_wheel(midi_channel, ((data_byte_3 <<= 7) | data_byte_2));
                break;
        }
    }
}

void handle_meta_event(uint8_t sub_type)
{
    uint32_t length = get_variable_len();  
    switch(sub_type)
    {
        case 0x2f:
            g_track_data[g_track].track_finished = true;
            break;
        case 0x51:
            g_qnote_duration = read_byte();
            g_qnote_duration = (g_qnote_duration << 8) + read_byte();
            g_qnote_duration = (g_qnote_duration << 8) + read_byte();
            g_roboplay_interface->update_refresh();
            break;
        default:
            for(uint32_t i = 0; i < length; i++) read_byte();
    }
}

void handle_sys_ex_event()
{
    uint8_t data;

    /* Skip SysEx data */
    do{
        data = read_byte();
    } while(data != 0xF7);
}

VOICE_DATA* get_voice(YRW801_WAVE_DATA *wave_data)
{
    VOICE_DATA *free_voice = &g_voice_data[0];
    VOICE_DATA *oldest_voice = &g_voice_data[0];
    for(uint8_t voice = 0; voice < NR_OF_WAVE_CHANNELS; voice++)
    {
        if(g_voice_data[voice].is_active)
        {
            if(g_voice_data[voice].activated < oldest_voice->activated)
                oldest_voice = &g_voice_data[voice];           
        }
        else
        {
            if(g_voice_data[voice].wave_data == wave_data)
            {
                free_voice = &g_voice_data[voice];
                break;
            }

            if(g_voice_data[voice].activated < free_voice->activated)
                free_voice = &g_voice_data[voice];
        }
    }

    /* If no free voice found, deactivate the 'oldest' */
    if(free_voice->is_active)
    {
        free_voice = oldest_voice;
        free_voice->activated = 0;

        free_voice->reg_misc &= ~OPL4_KEY_ON_BIT;
        g_roboplay_interface->opl_write_wave(OPL4_REG_MISC + free_voice->number, free_voice->reg_misc);
    }

    return free_voice;
}

void note_off(uint8_t note, uint8_t velocity, MIDI_CHANNEL_DATA *midi_channel)
{
    /* Velocity not used */
    velocity;

    for(uint8_t i = 0; i < NR_OF_WAVE_CHANNELS; i++)
    {
        VOICE_DATA *voice = &g_voice_data[i];
        if(voice->activated && voice->midi_channel == midi_channel && voice->note == note)
        {
            voice->is_active = false;

            voice->reg_misc &= ~OPL4_KEY_ON_BIT;
            g_roboplay_interface->opl_write_wave(OPL4_REG_MISC + voice->number, voice->reg_misc);
        }
    }
}

void note_on(uint8_t note, uint8_t velocity, MIDI_CHANNEL_DATA *midi_channel)
{
  g_roboplay_interface->set_segment(g_wave_segment);

  YRW801_REGION_DATA **regions = (YRW801_REGION_DATA**)DATA_SEGMENT_BASE;
  YRW801_WAVE_DATA *wave_data[2];

  VOICE_DATA* voice[2];
  uint8_t i = 0, voices = 0;

  if(midi_channel->drum_channel)
    wave_data[voices++] = &regions[0x80][note - 0x1A].wave_data;
  else
  {
    /* Determine the number of voices and voice parameters */
    YRW801_REGION_DATA *region = regions[midi_channel->instrument & 0x7F];

    while(region[i].key_min != 0xFF)
    {
      if(note >= region[i].key_min && note <= region[i].key_max)
      {
        wave_data[voices] = &region[i].wave_data;
        if(++voices >= 2) break;
      }
      i++;
    }
  }

  /* Allocate and initialize needed voices */
  for(i = 0; i < voices; i++)
  {
    voice[i] = get_voice(wave_data[i]);
    voice[i]->is_active = true;
    voice[i]->activated = g_MIDI_counter;

    voice[i]->midi_channel = midi_channel;
    voice[i]->note = note;
    voice[i]->velocity = velocity & 0x7F;
    voice[i]->wave_data = wave_data[i];
  }

  /* Set tone number (triggers header loading) */
  for(i = 0; i < voices; i++)
  {
    voice[i]->reg_f_number = (wave_data[i]->tone >> 8) & OPL4_TONE_NUMBER_BIT8;
    g_roboplay_interface->opl_write_wave(OPL4_REG_F_NUMBER + voice[i]->number, voice[i]->reg_f_number);

    g_roboplay_interface->opl_write_wave(OPL4_REG_TONE_NUMBER + voice[i]->number, wave_data[i]->tone & 0xFF);
  }

  /* Set parameters which can be set while loading */
  for(i = 0; i < voices; i++)
  {
    voice[i]->reg_misc = OPL4_LFO_RESET_BIT;
    update_pan(voice[i]);
    update_pitch(voice[i]);
    voice[i]->level_direct = OPL4_LEVEL_DIRECT_BIT;
    update_volume(voice[i]);
  }

  /* Wait for completion of loading */
  g_roboplay_interface->opl_wait_for_load();

  for(i = 0; i < voices; i++)
  {
    /* Set remaining parameters */
    g_roboplay_interface->opl_write_wave(OPL4_REG_ATTACK_DECAY1 + voice[i]->number, voice[i]->wave_data->reg_attack_decay1);
    g_roboplay_interface->opl_write_wave(OPL4_REG_LEVEL_DECAY2 + voice[i]->number, voice[i]->wave_data->reg_level_decay2);
    g_roboplay_interface->opl_write_wave(OPL4_REG_RELEASE_CORRECTION + voice[i]->number, voice[i]->wave_data->reg_release_correction);
    g_roboplay_interface->opl_write_wave(OPL4_REG_TREMOLO + voice[i]->number, voice[i]->wave_data->reg_tremolo);

    voice[i]->reg_lfo_vibrato = voice[i]->wave_data->reg_lfo_vibrato;

    if(!midi_channel->drum_channel)
      update_vibrato_depth(voice[i]);
  }

  /* Finally, switch on all voices */
  for(i = 0; i < voices; i++)
  {
      voice[i]->reg_misc = (voice[i]->reg_misc & 0x1F) | OPL4_KEY_ON_BIT;
      g_roboplay_interface->opl_write_wave(OPL4_REG_MISC + voice[i]->number, voice[i]->reg_misc);
  }    

  g_roboplay_interface->set_segment(g_track_data[g_track].segment);
}

void pitch_wheel(uint8_t midi_channel, uint8_t value)
{
    midi_channel;
    value;
}
void control_change(uint8_t midi_channel, uint8_t id, uint8_t value)
{
    switch(id)
    {
        case 10:
            /* Change stereo panning */
            if(midi_channel != DRUM_CHANNEL)
                g_midi_channel_data[midi_channel].panpot = (value - 0x40) >> 3;
            break;
    }
}

void channel_pressure(uint8_t midi_channel, uint8_t pressure)
{
    midi_channel;
    pressure;
}

void key_pressure(uint8_t midi_channel, uint8_t note, uint8_t pressure)
{
    midi_channel;
    note;
    pressure;
}

void program_change(uint8_t midi_channel, uint8_t program)
{
    g_midi_channel_data[midi_channel].instrument = program;
}

void update_pan(VOICE_DATA *voice)
{
    int8_t pan = voice->wave_data->panpot;

    if(!voice->midi_channel->drum_channel)
        pan += voice->midi_channel->panpot;
    if(pan < -7)
        pan = -7;
    else if(pan > 7)
        pan = 7;

    voice->reg_misc = (voice->reg_misc & ~OPL4_PAN_POT_MASK) | (pan & OPL4_PAN_POT_MASK);
    g_roboplay_interface->opl_write_wave(OPL4_REG_MISC + voice->number, voice->reg_misc);
}

void update_pitch(VOICE_DATA *voice)
{
    int32_t pitch = (voice->midi_channel->drum_channel) ? 0 : voice->note - 60;
    pitch = (pitch << 7);
    pitch = (pitch * voice->wave_data->key_scaling) / 100;
    pitch = pitch + 7680 + voice->wave_data->pitch_offset;

    if(pitch < 0) 
        pitch = 0;
    else if (pitch > 0x5FFF) 
        pitch = 0x5FFF;
    
    uint8_t octave = pitch / 0x600 - 8;
    uint16_t fnumber = g_wave_pitch_map[pitch % 0x600]; 

    g_roboplay_interface->opl_write_wave(OPL4_REG_OCTAVE + voice->number, (octave << 4) | ((fnumber >> 7) & 0x07));
    voice->reg_f_number = (voice->reg_f_number & OPL4_TONE_NUMBER_BIT8) | ((fnumber << 1) & OPL4_F_NUMBER_LOW_MASK);
    g_roboplay_interface->opl_write_wave(OPL4_REG_F_NUMBER + voice->number, voice->reg_f_number);
}

void update_volume(VOICE_DATA *voice)
{
    int16_t att = voice->wave_data->tone_attenuate;

    att += g_volume_table[voice->velocity];
    att = 0x7F - (0x7F - att) * (voice->wave_data->volume_factor) / 0xFE;
    att -= g_volume_boost;
    if(att < 0)
        att = 0;
    else if(att > 0x7E)
        att = 0x7E;

    g_roboplay_interface->opl_write_wave(OPL4_REG_LEVEL + voice->number, (att << 1) | voice->level_direct);
    voice->level_direct = 0;
}

void update_vibrato_depth(VOICE_DATA *voice)
{
    uint16_t depth;

    depth = (7 - voice->wave_data->vibrato) * (voice->midi_channel->vibrato & 0x7F);
    depth = (depth >> 7) + voice->wave_data->vibrato;
    voice->reg_lfo_vibrato &= ~OPL4_VIBRATO_DEPTH_MASK;
    voice->reg_lfo_vibrato |= depth & OPL4_VIBRATO_DEPTH_MASK;
    g_roboplay_interface->opl_write_wave(OPL4_REG_LFO_VIBRATO + voice->number, voice->reg_lfo_vibrato);
}
