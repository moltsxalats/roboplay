/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * rad.h
 *
 * RAD: Reality ADlib Tracker v1 player
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>

#define MAX_SEGMENT_INDEX 4

#define MAX_CHANNEL_NUMBER    9
#define MAX_INSTRUMENT_NUMBER 31
#define MAX_PATTERN_NUMBER    32
#define MAX_PATTERN_LINE      64

#define KEY_OFF 15

#define CMD_PORTAMENTO_UP   0x1
#define CMD_PORTAMENTO_DOWN 0x2
#define CMD_TONE_SLIDE      0x3
#define CMD_TONE_VOL_SLIDE  0x5
#define CMD_VOL_SLIDE       0xA
#define CMD_SET_VOL         0xC
#define CMD_JUMP_TO_LINE    0xD
#define CMD_SET_SPEED       0xF

#define FREQ_START  0x0156      /* Low end of frequency in each octave */
#define FREQ_END    0x02AE      /* High end of frequency in each octave */
#define FREQ_RANGE  FREQ_END - FREQ_START

#define OCTAVE_START 0x00
#define OCTAVE_END   0x07

#define PAN_SETTING_LEFT  0x10
#define PAN_SETTING_MID   0x30
#define PAN_SETTING_RIGHT 0x20

typedef struct
{
    char    id[16];
    uint8_t version;
    uint8_t flags;
} RAD_HEADER;

typedef struct 
{
    uint8_t data[11];
} RAD_INSTRUMENT;

typedef struct
{
    bool     tone_slide;
    int8_t   tone_slide_speed;
    uint16_t tone_slide_frequency;
    uint8_t  tone_slide_octave;

    int8_t port_slide;
    int8_t volume_slide;

    uint8_t instrument_number;

    uint16_t frequency;
    uint8_t  octave;

    uint8_t old_43;
    uint8_t old_A0;
    uint8_t old_B0;
} RAD_CHANNEL_DATA;

typedef struct
{
    bool    new_data;

    uint8_t note;
    uint8_t octave;

    uint8_t instrument_number;

    uint8_t command;
    uint8_t data;
} RAD_NOTE_DATA;

const uint16_t g_note_frequencies[] = 
{
    0x16B, 0x181, 0x198, 0x1B0, 0x1CA, 0x1E5,
    0x202, 0x220, 0x241, 0x263, 0x287, 0x2AE
};

const uint8_t g_channel_offsets[] =
{
    0x20, 0x21, 0x22, 0x28, 0x29, 0x2a, 0x30, 0x31, 0x32
};

RAD_HEADER *g_header;

uint8_t  *g_file_data;
uint8_t **g_patterns;

uint8_t *g_pattern_data;

uint8_t g_segment_list[MAX_SEGMENT_INDEX];
uint8_t g_segment_index;

RAD_INSTRUMENT *g_instruments[MAX_INSTRUMENT_NUMBER];

RAD_NOTE_DATA g_note_buffer[MAX_CHANNEL_NUMBER];

float g_hertz;

uint8_t *g_order_list;
uint8_t g_order_list_size;
uint8_t g_order_position;

uint8_t g_speed;
uint8_t g_speed_count;

uint8_t g_next_line;
uint8_t g_current_line;
uint8_t g_is_last_line;

int8_t  g_next_line_jump;

RAD_CHANNEL_DATA g_channel_data[MAX_CHANNEL_NUMBER];

uint8_t next_byte();

void fill_note_buffer();

void next_pattern();
void update_notes();

void play_note(uint8_t channel);

void load_instrument(uint8_t channel);
void load_instrument_volume(uint8_t channel);

void set_volume(uint8_t channel, uint8_t volume);

void tone_slide(uint8_t channel);
void tone_volume_slide(uint8_t channel);
void volume_slide(uint8_t channel);
