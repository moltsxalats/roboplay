/*
 * RoboPlay for MSX
 * Copyright (C) 2023 by RoboSoft Inc.
 *
 * rad.c
 *
 * RAD: Reality ADlib Tracker v1 player
 */

#include <string.h>

#include "player_interface.h"
#include "rad.h"

bool load(const char* file_name)
{    
    g_roboplay_interface->open(file_name, false);  
    g_file_data = (void *)DATA_SEGMENT_BASE;
    
    g_segment_index = 0;
    g_segment_list[g_segment_index++] = START_SEGMENT_INDEX;

    uint16_t bytes_read = 0;
    do
    {
        bytes_read = g_roboplay_interface->read(g_file_data, DATA_SEGMENT_SIZE);

        if(bytes_read == DATA_SEGMENT_SIZE)
        {
            g_segment_list[g_segment_index] = g_roboplay_interface->get_new_segment();
            g_roboplay_interface->set_segment(g_segment_list[g_segment_index]);

            g_segment_index++;
        }
    } while(bytes_read == DATA_SEGMENT_SIZE && g_segment_index <= MAX_SEGMENT_INDEX);

    g_roboplay_interface->close();

    g_roboplay_interface->set_segment(g_segment_list[0]);

    g_header = (RAD_HEADER *)g_file_data;
    if(strncmp(g_header->id, "RAD by REALiTY!!", 16) || g_header->version != 0x10) return false;

    uint8_t *data = g_file_data + sizeof(RAD_HEADER);

    /* Slow timer tune? */
    if(g_header->flags & 0x40) 
        g_hertz = 18.2;
    else
        g_hertz = 50.0;

    /* Skip any description */
    if(g_header->flags & 0x80)
    {
        while(*data) data++;
        data++;
    }

    /* Record pointers to the instruments */
    while(*data)
    {
        uint8_t instrument_number = *data++;

        g_instruments[instrument_number - 1] = (RAD_INSTRUMENT *)data;
        data += sizeof(RAD_INSTRUMENT);
    }
    data++;

    /* Get order list */
    g_order_list_size = *data++;
    g_order_list = data;
    data += g_order_list_size;

    /* Patterns */
    g_patterns = data;

    return true;
}

bool update()
{
    uint8_t i;

    if(!g_speed_count)
    {
        /* Reset any slide */
        for(uint8_t i = 0; i < MAX_CHANNEL_NUMBER; i++)
        {
            g_channel_data[i].port_slide = 0;
            g_channel_data[i].volume_slide = 0;
            g_channel_data[i].tone_slide = false;
        }
        
        g_next_line_jump = -1;

        if(g_current_line == g_next_line) 
        {
            for(i = 0; i < MAX_CHANNEL_NUMBER; i++)
            {
                if(g_note_buffer[i].new_data) 
                    play_note(i);
            }

            /* Was this the last line? */
            if(g_is_last_line) g_pattern_data = NULL;
        }

        g_speed_count = g_speed - 1;

        g_next_line++;
        if(g_next_line >= MAX_PATTERN_LINE || g_next_line_jump >= 0)
        {
            if(g_next_line_jump >= 0)
                g_next_line = g_next_line_jump;
            else
                g_next_line = 0;

            next_pattern();
        }

        g_current_line = *g_pattern_data & 0x7F;
        g_is_last_line = *g_pattern_data & 0x80;

        if(g_speed == 1 && g_pattern_data)
        {
            if(g_current_line == g_next_line)
                fill_note_buffer();
        }
    }
    else
    {
        g_speed_count--;

        /* Next update has a new line? */
        if(!g_speed_count && g_pattern_data)
        {
            if(g_current_line == g_next_line)
                fill_note_buffer();
        }
    }

    /* Now update notes */
    update_notes();

    return true;
}

void rewind(const uint8_t subsong)
{
    subsong;

    /* Configure OPL */
    g_roboplay_interface->opl_set_mode(ROBOPLAY_OPL_MODE_OPL3);

    g_roboplay_interface->opl_write_fm_1(0x01,  0x20); /* Allow waveforms */
    g_roboplay_interface->opl_write_fm_1(0x08,  0x00); /* No split point */
    g_roboplay_interface->opl_write_fm_1(0xBD,  0x00); /* No drums, etc. */

    /* Initial values */
    g_speed = g_header->flags & 0x3F;

    g_order_position = 0;
    g_speed_count    = 1;
    
    g_pattern_data = g_patterns[g_order_list[g_order_position]] + DATA_SEGMENT_BASE;
    g_next_line    = 0;

    g_current_line = *g_pattern_data & 0x7F;
    g_is_last_line = *g_pattern_data & 0x80;

    for(uint8_t i = 0; i < MAX_CHANNEL_NUMBER; i++)
    {
        g_channel_data[i].tone_slide = false;
        g_channel_data[i].tone_slide_speed = 0;
        g_channel_data[i].tone_slide_frequency= 0;
        g_channel_data[i].tone_slide_octave = 0;

        g_channel_data[i].port_slide = 0;
        g_channel_data[i].volume_slide = 0;

        g_channel_data[i].instrument_number = 0;

        g_channel_data[i].frequency = 0;
        g_channel_data[i].octave = 0;

        g_channel_data[i].old_43 = 0;
        g_channel_data[i].old_A0 = 0;
        g_channel_data[i].old_B0 = 0;
    }

    g_segment_index = 0;
}

void command(const uint8_t id)
{
    id;
}

float get_refresh()
{
    return g_hertz;
}

uint8_t get_subsongs()
{
    return 0;
}

char* get_player_info()
{
    return "Reality ADlib Tracker player V1.0 by RoboSoft Inc.";
}

char* get_title()
{
    return "-";
}

char* get_author()
{
    return "-";
}

char* get_description()
{
    return "-";
}

uint8_t next_byte()
{
    uint8_t byte = *g_pattern_data++;

    if((uint16_t)g_pattern_data == (DATA_SEGMENT_BASE + DATA_SEGMENT_SIZE))
    {
        g_segment_index++;
        g_roboplay_interface->set_segment(g_segment_list[g_segment_index]);
        g_pattern_data = (uint8_t *)DATA_SEGMENT_BASE;
    }

    return byte;
}

void fill_note_buffer()
{
    for(uint8_t i = 0; i < MAX_CHANNEL_NUMBER; i++)
        g_note_buffer[i].new_data = false;

    /* Move to first channel */
    next_byte();

    bool last_channel = false;
    while(!last_channel)
    {
        uint8_t data_0 = next_byte();
        uint8_t data_1 = next_byte();
        uint8_t data_2 = next_byte();

        last_channel = (data_0 & 0x80) ? true : false;

        uint8_t channel = data_0 & 0x7F;

        g_note_buffer[channel].new_data = true;

        g_note_buffer[channel].octave  = (data_1 & 0x70) >> 4;
        g_note_buffer[channel].note    = data_1 & 0x0F;

        g_note_buffer[channel].instrument_number = ((data_2 & 0xF0) >> 4) + ((data_1 & 0x80) >> 4);

        g_note_buffer[channel].command = data_2 & 0x0F;

        if(g_note_buffer[channel].command)
            g_note_buffer[channel].data = next_byte();
    }
}

void next_pattern()
{
    g_roboplay_interface->set_segment(g_segment_list[0]);

    g_order_position++;
    if(g_order_position >= g_order_list_size) g_order_position = 0;

    uint8_t pattern_number = g_order_list[g_order_position];
    if(pattern_number >= 0x80)
    {
        g_order_position = pattern_number - 0x80;
        pattern_number = g_order_list[g_order_position];
    }

    g_pattern_data = g_patterns[pattern_number];

    g_segment_index = 0;
    while((uint16_t)g_pattern_data > DATA_SEGMENT_SIZE)
    {
        g_pattern_data -= DATA_SEGMENT_SIZE;
        g_segment_index++;
    }

    g_pattern_data += DATA_SEGMENT_BASE;
    g_roboplay_interface->set_segment(g_segment_list[g_segment_index]);
}

void update_notes()
{
    for(uint8_t i = 0; i < MAX_CHANNEL_NUMBER; i++)
    {
        /* Process portamentos and tone slides */
        if(g_channel_data[i].port_slide || g_channel_data[i].tone_slide)
        {
            uint16_t frequency = g_channel_data[i].frequency;
            uint8_t  octave = g_channel_data[i].octave;

            if(g_channel_data[i].port_slide)
                frequency += g_channel_data[i].port_slide;

            if(g_channel_data[i].tone_slide)
                frequency += g_channel_data[i].tone_slide_speed;

            if(frequency < FREQ_START)
            {
                if(octave > OCTAVE_START)
                {
                    octave--;
                    frequency += FREQ_RANGE;
                }
                else frequency = FREQ_START;
            }
            else if(frequency > FREQ_END)
            {
                if(octave < OCTAVE_END)
                {
                    octave ++;
                    frequency -= FREQ_RANGE;
                }
                else frequency = FREQ_END;
            }

            if(g_channel_data[i].tone_slide)
            {
                 if(g_channel_data[i].tone_slide_speed >= 0)
                 {
                    if(octave > g_channel_data[i].tone_slide_octave || 
                      (octave == g_channel_data[i].tone_slide_octave && frequency >= g_channel_data[i].tone_slide_frequency))
                    {
                        frequency = g_channel_data[i].tone_slide_frequency;
                        octave = g_channel_data[i].tone_slide_octave;
                    }
                 }
                else
                {
                    if(octave < g_channel_data[i].tone_slide_octave || 
                      (octave == g_channel_data[i].tone_slide_octave && frequency <= g_channel_data[i].tone_slide_frequency))
                    {
                        frequency = g_channel_data[i].tone_slide_frequency;
                        octave = g_channel_data[i].tone_slide_octave;
                    }
                }
            }

            g_channel_data[i].frequency = frequency;
            g_channel_data[i].octave = octave;

            g_channel_data[i].old_A0 = (g_channel_data[i].frequency & 0xFF);
            g_channel_data[i].old_B0 = (g_channel_data[i].old_B0 & 0xE0) + (g_channel_data[i].octave << 2) + (g_channel_data[i].frequency >> 8);

            g_roboplay_interface->opl_write_fm_1(0xA0 + i, g_channel_data[i].old_A0);
            g_roboplay_interface->opl_write_fm_2(0xA0 + i, g_channel_data[i].old_A0 + 1);

            g_roboplay_interface->opl_write_fm_1(0xB0 + i, g_channel_data[i].old_B0);
            g_roboplay_interface->opl_write_fm_2(0xB0 + i, g_channel_data[i].old_B0);
        }

        /* Process volume slides */
        if(g_channel_data[i].volume_slide)
        {
            int8_t volume = (g_channel_data[i].old_43 & 0x3F) ^ 0x3F;
           
            volume += g_channel_data[i].volume_slide;
            if(volume < 0) volume = 0;

            set_volume(i, volume);
        }
    }

}

void play_note(uint8_t channel)
{
  if(g_note_buffer[channel].note)
  {
    if(g_note_buffer[channel].command == CMD_TONE_SLIDE)
    {
      if(g_note_buffer[channel].note < KEY_OFF)  
      {
        g_channel_data[channel].tone_slide_frequency = g_note_frequencies[g_note_buffer[channel].note - 1];
        g_channel_data[channel].tone_slide_octave = g_note_buffer[channel].octave;
      }
    }
    else
    {
      /* Key off old note */
      g_channel_data[channel].old_B0 = g_channel_data[channel].old_B0 & 0xDF;
      g_roboplay_interface->opl_write_fm_1(0xB0 + channel, g_channel_data[channel].old_B0);
      g_roboplay_interface->opl_write_fm_2(0xB0 + channel, g_channel_data[channel].old_B0);

      /* Load instrument (if any) */
      if(g_note_buffer[channel].instrument_number)
      {
        if(g_note_buffer[channel].instrument_number != g_channel_data[channel].instrument_number)
        {
          g_channel_data[channel].instrument_number = g_note_buffer[channel].instrument_number;
          load_instrument(channel);
        }
        else
          load_instrument_volume(channel);
      }

      if(g_note_buffer[channel].note < KEY_OFF)
      {           
        g_channel_data[channel].frequency = g_note_frequencies[g_note_buffer[channel].note - 1];
        g_channel_data[channel].octave = g_note_buffer[channel].octave;

        g_channel_data[channel].old_A0 = (g_channel_data[channel].frequency & 0xFF);
        g_channel_data[channel].old_B0 = 0x20 + (g_channel_data[channel].octave << 2) + (g_channel_data[channel].frequency >> 8);

        g_roboplay_interface->opl_write_fm_1(0xA0 + channel, g_channel_data[channel].old_A0);
        g_roboplay_interface->opl_write_fm_2(0xA0 + channel, g_channel_data[channel].old_A0 + 1);

        g_roboplay_interface->opl_write_fm_1(0xB0 + channel, g_channel_data[channel].old_B0);
        g_roboplay_interface->opl_write_fm_2(0xB0 + channel, g_channel_data[channel].old_B0);
      }
    }
  }

  switch(g_note_buffer[channel].command)
  {
    case CMD_PORTAMENTO_UP:
      g_channel_data[channel].port_slide = g_note_buffer[channel].data;
      break;
    case CMD_PORTAMENTO_DOWN:
      g_channel_data[channel].port_slide = -g_note_buffer[channel].data;
      break;
    case CMD_TONE_SLIDE:
      tone_slide(channel);
      break;
    case CMD_TONE_VOL_SLIDE:
      tone_volume_slide(channel);
      break;
    case CMD_VOL_SLIDE:
      volume_slide(channel);
      break;
    case CMD_SET_VOL:
      set_volume(channel, g_note_buffer[channel].data);
      break;
    case CMD_JUMP_TO_LINE:
      if(g_note_buffer[channel].data < MAX_PATTERN_LINE)
        g_next_line_jump = g_note_buffer[channel].data;
      break;
    case CMD_SET_SPEED:
      g_speed = g_note_buffer[channel].data;
      break;
  }
}

void load_instrument(uint8_t channel)
{
    g_roboplay_interface->set_segment(g_segment_list[0]);

    RAD_INSTRUMENT *instrument = g_instruments[g_channel_data[channel].instrument_number - 1];
    g_channel_data[channel].old_43 = instrument->data[2];

    uint8_t reg = g_channel_offsets[channel];
    uint8_t offset = 0;
    for(uint8_t i = 0; i < 4; i++)
    {
        g_roboplay_interface->opl_write_fm_1(reg, instrument->data[offset + 1]);
        g_roboplay_interface->opl_write_fm_2(reg, instrument->data[offset + 1]);
        reg += 0x03;
        g_roboplay_interface->opl_write_fm_1(reg, instrument->data[offset]);
        g_roboplay_interface->opl_write_fm_2(reg, instrument->data[offset]);
        reg += 0x20 - 0x03;

        offset += 2;
    }

    reg += 0x40;
    g_roboplay_interface->opl_write_fm_1(reg, instrument->data[offset + 2]);
    g_roboplay_interface->opl_write_fm_2(reg, instrument->data[offset + 2]);
    reg += 0x03;
    g_roboplay_interface->opl_write_fm_1(reg, instrument->data[offset + 1]);
    g_roboplay_interface->opl_write_fm_2(reg, instrument->data[offset + 1]);

    g_roboplay_interface->opl_write_fm_1(0xC0 + channel, instrument->data[offset] + PAN_SETTING_LEFT);
    g_roboplay_interface->opl_write_fm_2(0xC0 + channel, instrument->data[offset] + PAN_SETTING_RIGHT);

    g_roboplay_interface->set_segment(g_segment_list[g_segment_index]);
}

void load_instrument_volume(uint8_t channel)
{
    g_roboplay_interface->set_segment(g_segment_list[0]);

    RAD_INSTRUMENT *instrument = g_instruments[g_channel_data[channel].instrument_number - 1];
    g_channel_data[channel].old_43 = instrument->data[2];

    g_roboplay_interface->opl_write_fm_1(g_channel_offsets[channel] + 0x23, g_channel_data[channel].old_43);
    g_roboplay_interface->opl_write_fm_2(g_channel_offsets[channel] + 0x23, g_channel_data[channel].old_43);   

    g_roboplay_interface->set_segment(g_segment_list[g_segment_index]);
}

void set_volume(uint8_t channel, uint8_t volume)
{
    if(volume > 0x3F) volume = 0x3F;
    g_channel_data[channel].old_43 = (g_channel_data[channel].old_43 & 0xC0) + (volume ^ 0x3F);

    g_roboplay_interface->opl_write_fm_1(g_channel_offsets[channel] + 0x23, g_channel_data[channel].old_43);
    g_roboplay_interface->opl_write_fm_2(g_channel_offsets[channel] + 0x23, g_channel_data[channel].old_43);
}

void tone_slide(uint8_t channel)
{
    if(g_note_buffer[channel].data) g_channel_data[channel].tone_slide_speed = g_note_buffer[channel].data;
    g_channel_data[channel].tone_slide = true;

}

void tone_volume_slide(uint8_t channel)
{
    volume_slide(channel);
    g_channel_data[channel].tone_slide = true;
}

void volume_slide(uint8_t channel)
{
    if(g_note_buffer[channel].data > 50)
        g_channel_data[channel].volume_slide = (g_note_buffer[channel].data - 50);
    else
        g_channel_data[channel].volume_slide = -g_note_buffer[channel].data;
}


